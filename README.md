# UnityGB4BB #

### What is this? ###

This is a GameBoy Emulator designed by Takohi (you can read his forum post here: http://forum.unity3d.com/threads/unitygb-emulator-game-boy-for-unity-released.245974/).  
Key changes:
- Made it mobile friendly
- Added some emulator browsing functionality
- Added fancy new graphics

For more information and screen captures:
http://www.filearchivehaven.com/2014/08/17/proud-to-announce-another-gameboy-emulator-for-blackberry-10-unitygb4bb10/

Follow the original repo on Github here:
https://github.com/Takohi-Jona/unity-gb